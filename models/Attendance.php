<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "attendance".
 *
 * @property int $id
 * @property int $user_id
 * @property string $time
 * @property int $status
 * @property string $note
 */
class Attendance extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'attendance';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['name'], 'string'],
                [['user_id', 'status'], 'integer'],
                [['time'], 'safe'],
                [['note'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Nama',
            'user_id' => 'ID Pengguna',
            'time' => 'Waktu',
            'status' => 'Status',
            'note' => 'Catatan',
            'photo'=>'Foto',
        ];
    }

}
