<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal".
 *
 * @property int $id
 * @property string $institution_name
 * @property string $institution_email
 * @property string $institution_phone
 * @property int $city_id
 * @property string $address
 * @property int $applicant_id
 * @property string $start_date
 * @property string $end_date
 * @property resource $referral_letter
 * @property resource $proposal
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Proposal extends \yii\db\ActiveRecord {

    public $proposal_file;
    public $letter_file;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'proposal';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['institution_name', 'institution_email', 'institution_phone', 'city_id', 'address', 'applicant_id', 'start_date', 'end_date', 'status', 'created_at', 'updated_at', 'proposal_file', 'letter_file'], 'required'],
                [['city_id', 'applicant_id', 'status', 'created_at', 'updated_at'], 'integer'],
                [['start_date', 'end_date'], 'safe'],
                [['institution_name'], 'string', 'max' => 50],
                [['institution_email'], 'email'],
                [['institution_phone'], 'string', 'max' => 15],
                [['address'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'institution_name' => 'Nama',
            'institution_email' => 'Email',
            'institution_phone' => 'Nomor Telepon',
            'city_id' => 'Kota',
            'address' => 'Alamat Lengkap',
            'applicant_id' => 'ID Pelamar',
            'start_date' => 'Waktu Mulai Magang',
            'end_date' => 'Waktu Berakhir Magang',
            'referral_letter' => 'Surat Pengantar',
            'proposal' => 'Proposal',
            'status' => 'Status',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diperbarui Pada',
            'letter_file' => 'Surat Pengantar',
            'proposal_file' => 'Porposal',
        ];
    }

}
