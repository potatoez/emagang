<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_experiences".
 *
 * @property int $id
 * @property int $user_id
 * @property string $institution_name
 * @property string $position
 * @property string $year
 * @property int $create_at
 * @property int $update_at
 */
class WorkExperience extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'work_experiences';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['work_name', 'position', 'year'], 'required'],
                [['user_id', 'create_at', 'update_at'], 'integer'],
                [['year'], 'string', 'max' => 4],
                [['institution_name', 'position'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'ID Pengguna',
            'institution_name' => 'Nama Tempat Kerja',
            'position' => 'Jabatan',
            'year' => 'Tahun',
            'create_at' => 'Dibuat Pada',
            'update_at' => 'Diubah Pada',
        ];
    }

}
