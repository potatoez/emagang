<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrganizationExperience;

/**
 * OrganizationExperienceSearch represents the model behind the search form of `app\models\OrganizationExperience`.
 */
class OrganizationExperienceSearch extends OrganizationExperience {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'user_id', 'create_at', 'update_at'], 'integer'],
                [['organization_name', 'position', 'year'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OrganizationExperience::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => Yii::$app->user->id,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'organization_name', $this->organization_name])
                ->andFilterWhere(['like', 'position', $this->position])
                ->andFilterWhere(['like', 'year', $this->year]);

        return $dataProvider;
    }

}
