<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "applicants".
 *
 * @property int $id
 * @property int $proposal_id 
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 */
class Applicant extends \yii\db\ActiveRecord {

    public $email;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'applicants';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['proposal_id', 'email', 'user_id',], 'required'],
                [['proposal_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
                [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'proposal_id', 'ID Proposal',
            'user_id' => 'User ID',
            'email'=>'Email',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

}
