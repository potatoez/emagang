<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "biodata".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $sex
 * @property int $birth_place_id
 * @property string $birth_date
 * @property string $phone
 * @property int $city_id
 * @property string $sub_district
 * @property string $address
 * @property string $expertise
 * @property string $photo
 * @property string $id_card_image
 * @property string $qr_code
 * @property int $id_card_type
 * @property string $id_number
 * @property int $created_at
 * @property int $updated_at
 */
class Biodata extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $photo_profile;
    public $scan_id_card;

    public static function tableName() {
        return 'biodata';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['user_id', 'name', 'sex', 'birth_place_id', 'birth_date', 'phone', 'city_id', 'sub_district', 'address', 'expertise', 'photo', 'qr_code', 'id_card_type', 'id_number','id_card_image',], 'required'],
                [['photo_profile', 'scan_id_card'], 'required'],
                [['user_id', 'birth_place_id', 'city_id', 'id_card_type', 'created_at', 'updated_at'], 'integer'],
                [['birth_date'], 'date'],
                [['photo_profile'], 'safe'],
                [['photo_profile'], 'file', 'extensions' => 'jpg, gif, png'],
                [['scan_id_card'], 'safe'],
                [['scan_id_card'], 'file', 'extensions' => 'jpg, gif, png'],
                [['name'], 'string', 'max' => 35],
                [['sex'], 'string', 'max' => 1],
                [['phone'], 'string'],
                [['sub_district'], 'string', 'max' => 25],
                [['address', 'expertise'], 'string', 'max' => 100],
                [['qr_code'], 'string', 'max' => 11],
                [['id_number'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Nama Lengkap',
            'sex' => 'Jenis Kelamin',
            'birth_place_id' => 'Kota Kelahiran',
            'birth_date' => 'Tanggal Lahir',
            'phone' => 'Nomor HP / WhatsApp',
            'city_id' => 'Kota',
            'sub_district' => 'Kecamatan',
            'address' => 'Alamat Rumah',
            'expertise' => 'Keahlian',
            'photo_profile' => 'Foto',
            'photo'=>'Foto',
            'qr_code' => 'Qr Code',
            'id_card_type' => 'Jenis Kartu Identitas',
            'id_number' => 'Nomor Identitas',
            'scan_id_card' => 'Scan Kartu Identitas',
            'id_card_image'=>'Scan Kartu Identitas',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

}
