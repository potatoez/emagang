<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Attendance;
use yii\db\Query;
use app\models\Status;
/**
 * AttendanceSearch represents the model behind the search form of `app\models\Attendance`.
 */
class AttendanceSearch extends Attendance {

    public $name;
    public $photo;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['name', 'photo'], 'string'],
                [['id', 'user_id', 'status'], 'integer'],
                [['time', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = $this->getUserData();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'user_id' => $this->user_id,
            'time' => $this->time,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(
                ['like', 'note', $this->note],
                ['like', 'name', $this->name]
                );

        return $dataProvider;
    }

    public function getUserData() {
        $subQuery2 = (new Query())
                ->select(['max(time)'])
                ->from('attendance')
                ->groupBy('user_id');

        $subQuery1 = (new Query())
                ->select(['*'])
                ->from('attendance')
                ->where(['in', 'time', $subQuery2]);

        $query = (new Query())
                ->select(['*'])
                ->from('biodata')
                ->leftJoin(['temp' => $subQuery1], 'biodata.user_id=temp.user_id');

//        print '<pre>';
//        print_r($query);
//        print '</pre>';
//        exit;
        return $query;
    }

}
