<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organization_experiences".
 *
 * @property int $id
 * @property int $user_id
 * @property string $organization_name
 * @property string $position
 * @property string $year
 * @property int $create_at
 * @property int $update_at
 */
class OrganizationExperience extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'organization_experiences';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['organization_name', 'position', 'year'], 'required'],
                [['user_id', 'create_at', 'update_at'], 'integer'],
                [['organization_name', 'position'], 'string', 'max' => 255],
                [['year'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'ID Pengguna',
            'organization_name' => 'Nama Organisasi',
            'position' => 'Jabatan',
            'year' => 'Tahun',
            'create_at' => 'Dibuat Pada',
            'update_at' => 'Diubah Pada',
        ];
    }

}
