<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Proposal;
use app\models\ProposalSearch;
use app\models\Assignment;

/**
 * Site controller
 */
class AdministratorController extends Controller
{

    /**
     * @inheritdoc
     */
     public function actions() {
         return [
             'error' => [
                 'class' => 'yii\web\ErrorAction',
             ],
         ];
     }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $except = Assignment::find()->select('user_id')->groupBy('user_id')->count();
        $model  = Proposal::find();

        $aktif = $model->where(['status' => 10])->count();
        $validasi = $aktif - $except;
        $validasi < 0 ? $validasi = 0 : $validasi;
        $wait       = $model->where(['status' => 1])->count();
        $ditolak = $model->where(['status' => 5])->count();
        $hariini = $model->where(['status' => 0])->andWhere(['between', 'created_at', strtotime(date("Y-m-d 00:00:00")), strtotime(date("Y-m-d 23:59:59"))])->count();

        $data = [$validasi, $ditolak, $wait, $hariini];
        return $this->render('index',[
            'data' => $data,
        ]);
    }

    public function actionValidasi(){
        $model = Proposal::find()->where(['auth_key' => $b])->one();
        
        if($model->load(\Yii::$app->request->post())) {
            
            if($model->is_valid == 1) {
                $model->status = 10;
            }
            
            if($model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('validasi', [
                    'model' => $model,
                ]);
            }
            
        } else {
            return $this->render('validasi', [
                'model' => $model,
            ]);
        }
    }

    public function actionView(){
        $model = User::find()->where(['auth_key' => $b])->one();
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionDelete(){
        $flag = true;
        
        if($this->findModel($id)->auth_key != $b) {
            return $this->redirect(['index']);
        }
        
        $transaction = \Yii::$app->db->beginTransaction();
        
        try {
            $delAnggota = \app\models\UserAnggota::find()->where(['id_ntfs_user' => $id])->all();
            
            foreach ($delAnggota as $anggota) {
                $flag = $anggota->delete() && $flag;
            }
            
            $imageName = $this->findModel($id)->foto_ketua;
            
            $flag = $this->findModel($id)->delete() && $flag;
            
            if($flag) {
                $transaction->commit();
                unlink(\Yii::$app->basePath. '\web\img\user' . '\\' . $imageName) && $flag;
            }
        } catch (Exception $ex) {
            $transaction->rollBack();
        }
    }

    public function actionRequestResetPassword($user){
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'nama_tim' => $tim,
        ]);

        if (!$user) {
            Yii::$app->session->setFlash('error', 'Nama Tim tidak ditemukan.');
            return $this->goHome();
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        Yii::$app->session->setFlash('success', 'Token berhasil digenerate');

        return $this->render('request-reset-password', [
            'token' => $user->password_reset_token,
        ]);
    }

    
    public function actionActionPage($a, $label) {
        
        $searchModel = new ProposalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // echo "<pre>";
        // var_dump($dataProvider);
        // exit();
        // echo "</pre>";
        return $this->render('valid', [
            'a' => $a,
            'label' => $label,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

        
    protected function findModel($id) {
        if (($model = Proposal::findOne($id)) !== null) {
            return $model;
        }
    }
}
