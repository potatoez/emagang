<?php

namespace app\controllers;

use Yii;
use app\models\OrganizationExperience;
use app\models\OrganizationExperienceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Model;

/**
 * OrganizationExperienceController implements the CRUD actions for OrganizationExperience model.
 */
class OrganizationExperienceController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrganizationExperience models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new OrganizationExperienceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrganizationExperience model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrganizationExperience model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $mModel = new OrganizationExperience();
        $models = [new OrganizationExperience()];
        if ($mModel->load(Yii::$app->request->post())) {
            $models = Model::createMultiple(OrganizationExperience::classname());
            Model::loadMultiple($models, \Yii::$app->request->post());

            $valid = Model::validateMultiple($models, [
                        'organization_name',
                        'position',
                        'year',
            ]);


            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $flag = false;
                    foreach ($models as $model) {
                        $model->user_id = Yii::$app->user->id;
                        $model->create_at = time();
                        $model->update_at = time();
                        if (!($flag = $model->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect('index');
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    return $this->render('create', [
                                'models' => $model,
                    ]);
                }
            }
        } else {
            return $this->render('create', [
                        'models' => (empty($models)) ? [new OrganizationExperience] : $models,
            ]);
        }
    }

    /**
     * Updates an existing OrganizationExperience model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->update_at=time();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrganizationExperience model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrganizationExperience model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrganizationExperience the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = OrganizationExperience::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
