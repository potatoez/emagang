<?php

namespace app\controllers;

use Yii;
use app\models\Attendance;
use app\models\AttendanceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Biodata;
use app\models\BiodataSearch;
use yii\db\Query;
/**
 * AttendanceController implements the CRUD actions for Attendance model.
 */
class AttendanceController extends Controller {

    public static function getUserData() {
        $subQuery2 = (new Query())
                ->select(['max(time)'])
                ->from('attendance')
                ->groupBy('user_id');

        $subQuery1 = (new Query())
                ->select(['*'])
                ->from('attendance')
                ->where(['in', 'time', $subQuery2]);

        $query = (new Query())
                ->select(['*'])
                ->from('biodata')
                ->leftJoin(['temp' => $subQuery1], 'biodata.user_id=temp.user_id')
                ->all();
//        print '<pre>';
//        print_r($query);
//        print '</pre>';
//        exit;
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Attendance models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new AttendanceSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Attendance model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Attendance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Attendance();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Attendance model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Attendance model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect('index');
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Attendance::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAttend($id) {
        $model=new Attendance();
        
        $model->user_id=$id;
        print '<pre>';
        print_r(new DateTime());
        print '</pre>';

        return $this->redirect(['index']);
    }

    public function actionOfficial($id) {
        $model = new Attendance();

        $model->user_id = $id;
        $model->status = 9;
        $model->time = '2018-05-01 13:14:15';

        $model->save(false);
        return $this->redirect('index');
    }

    public function actionPersonalPermission($id) {

        return $this->redirect(['index']);
    }

    public function actionReturn($id) {

        return $this->redirect(['index']);
    }

    public function actionGoHome($id) {
        return $this->redirect(['index']);
    }

    public function actionCancel($id) {
        return $this->redirect(['index']);
    }

    public function actionSick($id) {
        return $this->redirect(['index']);
    }

}
