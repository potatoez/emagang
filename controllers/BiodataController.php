<?php

namespace app\controllers;

use Yii;
use app\models\Biodata;
use app\models\BiodataSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\City;

/**
 * BiodataController implements the CRUD actions for Biodata model.
 */
class BiodataController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Biodata models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BiodataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Biodata::findOne(['user_id' => Yii::$app->user->id]) != null) {
            return $this->render('view', [
                        'model' => $this->findModel(['user_id' => Yii::$app->user->id]),
            ]);
            // }else if("admin"){
            //     return $this->render('index', [
            //         'searchModel' => $searchModel,
            //         'dataProvider' => $dataProvider,
            //     ]);
        } else {
            return $this->redirect('biodata/create');
        }
    }

    /**
     * Displays a single Biodata model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Biodata model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Biodata();
        if (Biodata::findOne(['user_id' => Yii::$app->user->id]) != null) {
            return $this->redirect('index');
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
               
            $model->qr_code=Yii::$app->security->generateRandomString();
            
            $model->created_at = time();
            $model->updated_at = time();

            $photo = UploadedFile::getInstance($model, 'photo_profile');
            $tmp1 = explode('.', $photo->name);
            $ext1 = end($tmp1);
            $model->photo = 'images/users/photos/' . Yii::$app->security->generateRandomString() . ".{$ext1}";

            $id_card_image = UploadedFile::getInstance($model, 'scan_id_card');
            $tmp2 = explode('.', $id_card_image->name);
            $ext2 = end($tmp2);
            $model->id_card_image = 'images/users/id_cards/' . Yii::$app->security->generateRandomString() . ".{$ext2}";
            
            if ($model->save(false)) {
                $photo->saveAs($model->photo);
                $id_card_image->saveAs($model->id_card_image);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Biodata model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            $model->updated_at = time();

            unlink($model->photo);
            unlink($model->id_card_image);
            
            $photo = UploadedFile::getInstance($model, 'photo_profile');
            $tmp1 = explode('.', $photo->name);
            $ext1 = end($tmp1);
            $model->photo = 'images/users/photos/' . Yii::$app->security->generateRandomString() . ".{$ext1}";

            $id_card_image = UploadedFile::getInstance($model, 'scan_id_card');
            $tmp2 = explode('.', $id_card_image->name);
            $ext2 = end($tmp2);
            $model->id_card_image = 'images/users/id_cards/' . Yii::$app->security->generateRandomString() . ".{$ext2}";

            if ($model->save(false)) {
                $photo->saveAs($model->photo);
                $id_card_image->saveAs($model->id_card_image);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                
            }
             return $this->redirect('index');
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Biodata model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        unlink($model->photo);
        unlink($model->id_card_image);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Biodata model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Biodata the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Biodata::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function date_converter($date) {
        $date = str_replace('/', '-', $date);
        return date('Y-m-d', strtotime($date));
    }

}
