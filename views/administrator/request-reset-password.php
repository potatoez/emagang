<?php

use yii\helpers\Html;

?>
<div class="square">
    <h4 class="page--header">
    HASIL GENERATE TOKEN
    </h4>

    <div class="alert alert-info" role="alert">
      <strong>Klik kanan >> Copy link</strong>, kemudian kirimkan ke peserta yang merequest.
      <p style="margin: 12px 0">
        Token: <?= Html::a($token, ['/site/reset-password', 'token' => $token], ['class' => 'alert-link']) ?>
      </p>
    </div>

    <div class="alert alert-danger" role="alert">
      Token berlaku sampai <a class="alert-link"><?= date('d-m-Y H:i:s', strtotime('+24 HOURS')) ?></a>.
    </div>
</div>
