<?php

use yii\helpers\Html;

$label = ['disetujui', 'ditolak', 'menunggu validasi', 'hari ini'];

$color = ['green', 'red', 'yellow', 'aqua'];
$icon = ['ios-checkmark-outline','ios-personadd','android-warning','ios-information-outline'];

$this->title = 'Dashboard Admin';
?>

<div class="row">
<?php for($i = 0; $i< 4 ; $i++){ ?>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-<?= $color[$i] ?>">

            <div class="inner">
                <h3><?= $data[$i] ?> DATA</h3>
                <p><?= ucwords($label[$i]) ?></p>
            </div>
            <div class="icon">
                <i class="ion ion-<?= $icon[$i]?>"></i>
            </div>

            <?= Html::a('Selengkapnya <i class="fa fa-arrow-circle-right"></i>', ['action-page', 'a' => $i, 'label' => $label[$i]], ['class' => 'small-box-footer'])?>
        </div>
    </div>
    <?php } ?>
</div>
