<?php

/**
 * @Author: SSimg
 * @Date:   2017-06-04 18:17:24
 * @Last Modified by:   SSimg
 * @Last Modified time: 2017-06-18 21:57:41
 */
use yii\helpers\Html;

$this->title = $label;
$this->params['breadcrumbs'][] = ['label' => 'Admin Pusat', 'url' => ['/administrator']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="square">

    <h4 class="page--header">

<?= Html::encode($this->title) ?>

    </h4>

    <!--
        $a adalah filter kategori data
        0 => Sudah Terdaftar; 1 => Menuggu Validasi; 2 => Belum Membayar; 3 => Pendaftar Hari Ini;
    -->

<?php
if ($a == 0) {
    echo $this->render('_table1', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
} elseif ($a == 1) {
    echo $this->render('_table2', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
} elseif ($a == 2) {
    echo $this->render('_table3', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
} else {
    echo $this->render('_table4', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
}
?>

</div>