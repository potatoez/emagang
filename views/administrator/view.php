<?php

use kartik\detail\DetailView;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo '<pre>';
//print_r(\Yii::$app->request->BaseUrl.'/img/'.$model->foto_ketua);
//echo '</pre>';
//exit();

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
//        'id',
        'nama_tim',
        [                       
            'label' => 'Foto Ketua',
            'value' =>  !empty($model->foto_ketua) ? \Yii::$app->request->BaseUrl.'/img/user/'.$model->foto_ketua : "Foto tidak ditemukan",
            'format' => ['image',['width'=>'150','height'=>'200']],
        ],
        'nama_ketua',
        'email',
        'sekolah',
        'kode_verifikasi',
        'nama_rekening_transfer',
        [                       
            'label' => 'Bukti Transfer',
            'value' =>  !empty($model->bukti_transfer) ? \Yii::$app->request->BaseUrl.'/img/user/'.$model->bukti_transfer : "Bukti tidak ditemukan",
            'format' => ['image',['width'=>'150','height'=>'200']],
        ],
    ],
]);