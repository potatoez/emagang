<?php

/**
 * @Author: SSimg
 * @Date:   2017-06-11 18:23:32
 * @Last Modified by:   SSimg
 * @Last Modified time: 2017-07-06 08:52:36
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

Pjax::begin();
?>


    <div class="proposal-index">

        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'institution_name',
            'institution_email:email',
            'institution_phone',
            'city_id',
            //'address',
            //'applicant_id',
            //'start_date',
            //'end_date',
            //'referral_letter',
            //'proposal',
            //'status',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view} {delete}',
            //                'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="fa fa-id-card"></span> Lihat', $url, [
                                'class' => 'btn btn-primary btn-xs',
                                'data-toggle' => 'tooltip',
                                'title' => 'Lihat Rincian ' . $model->id,
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="fa fa-close"></span> Batalkan', $url, [
                                'class' => 'btn btn-danger btn-xs',
                                'data-toggle' => 'tooltip',
                                'data-method' => 'post',
                                'title' => 'Batalkan ' . $model->id,
                    ]);
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url = ['/profil/index', 'id' => $model->id];
                    return $url;
                }
                if ($action === 'delete') {
                    $url = ['delete', 'id' => $model->id, 'b' => $model->id];
                    return $url;
                }
            }
        ],
    ],
    ]); ?>
    </div>

    <!-- <div class="table-responsive">
    <php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'nama_tim',
            'username',
            'nama_ketua',
            'sekolah',
//            [
//                'attribute' => 'nisn',
//                'label' => 'NISN',
//                'value' => 'nisn',
//            ],
            [
                'attribute' => 'created_at',
                'label' => 'TANGGAL DAFTAR',
                'format' => 'datetime',
                'value' => 'created_at',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
//                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="fa fa-id-card"></span> Lihat', $url, [
                                    'class' => 'btn btn-primary btn-xs',
                                    'data-toggle' => 'tooltip',
                                    'title' => 'Lihat Rincian ' . $model->nama_tim,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="fa fa-close"></span> Batalkan', $url, [
                                    'class' => 'btn btn-danger btn-xs',
                                    'data-toggle' => 'tooltip',
                                    'data-method' => 'post',
                                    'title' => 'Batalkan ' . $model->nama_tim,
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url = ['/profil/index', 'id' => $model->id];
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url = ['delete', 'id' => $model->id, 'b' => $model->auth_key];
                        return $url;
                    }
                }
            ],
        ],
    ]);
    ?>
</div> -->


    <?php
Pjax::end();
?>