<?php

require_once '../../vendor/autoload.php';
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Response\QrCodeResponse;

// Create a basic QR code
$qrCode = new QrCode('524');
$qrCode->setSize(100);

// Set advanced options
$qrCode->setMargin(5);
$qrCode->setEncoding('UTF-8');
$qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH);
$qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
$qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
$qrCode->setLogoWidth(150);
$qrCode->setValidateResult(false);

header('Content-Type: '.$qrCode->getContentType());
echo $qrCode->writeString();
?>