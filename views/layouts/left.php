<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: auto; margin-bottom: 10px;">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <!-- <p>User Profile</p> -->
                <p style="white-space: normal"><?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->username : 'eMagang' ?></p>
                <?php if (!Yii::$app->user->isGuest) echo '<a href="#"><i class="fa fa-circle text-success"></i> Online</a>' ?>
            </div>
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                            ['label' => 'Dashboard', 'options' => ['class' => 'header']],
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                            ['label' => 'Pendaftaran Magang', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                            ['label' => 'Biodata Diri', 'icon' => 'dashboard', 'url' => ['/biodata']],
                            [
                            'label' => 'Daftar Riwayat Hidup',
                            'icon' => 'share',
                            'url' => '#',
                            'items' => [
                                    ['label' => 'Pengalaman Organisasi', 'icon' => 'dashboard', 'url' => ['/organization-experience']],
                                    ['label' => 'Pengalaman Kerja', 'icon' => 'dashboard', 'url' => ['/work-experience']],
                            ],
                        //                                    [
//                                    'label' => 'Level One',
//                                    'icon' => 'circle-o',
//                                    'url' => '#',
//                                    'items' => [
//                                            ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
//                                            [
//                                            'label' => 'Level Two',
//                                            'icon' => 'circle-o',
//                                            'url' => '#',
//                                            'items' => [
//                                                    ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                                    ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                            ],
//                                        ],
//                                    ],
//                                ],
                        ],
                            ['label' => 'Pendaftaran Magang', 'icon' => 'dashboard', 'url' => ['/proposal']],
                            ['label' => 'Kehadiran', 'icon' => 'dashboard', 'url' => ['/attendance']],
                          ['label' => 'Proposal', 'icon' => 'dashboard', 'url' => ['/proposal']],
                        ['label' => 'Cetak Tanda Pengenal', 'icon' => 'dashboard', 'url' => ['/proposal']],
                        ['label' => 'Penilaian Magang', 'icon' => 'dashboard', 'url' => ['/proposal']],
                        ['label' => 'Cetak Sertifikat', 'icon' => 'dashboard', 'url' => ['/proposal']],
                    ],
                ]
        )
        ?>

    </section>

</aside>
