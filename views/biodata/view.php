<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\City;

/* @var $this yii\web\View */
/* @var $model app\models\Biodata */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Biodatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biodata-view">

    <h1><?php //Html::encode($this->title)                            ?></h1>

    <p>
        <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
//            'user_id',
            'name',
                ['attribute' => 'sex',
                'value' => function ($model) {
                    $sex = ['M' => Yii::t('app', 'Laki-laki'), 'F' => Yii::t('app', 'Perempuan')];
                    return $sex[$model->sex];
                },],
                ['attribute' => 'birth_place_id',
                'value' => City::findOne($model->city_id)->city,],
                ['attribute' => 'birth_date',
                'value' => function($model) {
                    return date_format(new DateTime($model->birth_date), "Y-m-d");
                },],
            'phone',
            'city_id',
            'sub_district',
            'address',
            'expertise',
                ['attribute' => 'photo',
                'value' => $model->photo,
                'format' => ['image', ['width' => '100', 'height' => '100']],],
//            'qr_code',
            ['attribute' => 'id_card_type',
                'value' => app\models\IdentityCardType::findOne($model->id_card_type)->type,],
            'id_number',
                ['attribute' => 'id_card_image',
                'value' => $model->id_card_image,
                'format' => ['image', ['width' => '100', 'height' => '100']],],
        ],
    ])
    ?>

</div>
