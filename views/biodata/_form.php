<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\models\City;
use app\models\IdentityCardType;
use yii\helpers\ArrayHelper;

$city = ArrayHelper::map(City::find()->select(['id', 'city'])->all(), 'id', 'city');
$identity_card_type = ArrayHelper::map(IdentityCardType::find()->select(['id', 'type'])->all(), 'id', 'type');
/* @var $this yii\web\View */
/* @var $model app\models\Biodata */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="biodata-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sex')->radioList(array('M' => 'Laki-laki', 'F' => 'Perempuan')); ?>

    <?=
    $form->field($model, 'birth_place_id')->widget(Select2::classname(), [
        'data' => $city,
        'options' => ['placeholder' => 'Pilih Kabupaten atau Kota'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])
    ?>

    <?= $form->field($model, 'birth_date')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'yyyy-mm-dd'],
        'removeButton' => false,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
    ]);
    ?>

    <?= $form->field($model, 'phone')->textInput(
            ['placeholder' => '+62xxxxxxxxxxx']
    )
    ?>

    <?=
    $form->field($model, 'city_id')->widget(Select2::classname(), [
        'data' => $city,
        'options' => ['placeholder' => 'Pilih Kabupaten atau Kota'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])
    ?>




    <?= $form->field($model, 'sub_district')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'expertise')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'photo_profile')->widget(FileInput::className(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
             'showUpload' => false
        ]
    ])
    ?>

    <?=
    $form->field($model, 'id_card_type')->widget(Select2::classname(), [
        'data' => $identity_card_type,
        'options' => ['placeholder' => 'Pilih Jenis Kartu Identitas'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])
    ?>

    <?= $form->field($model, 'id_number')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'scan_id_card')->widget(FileInput::className(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
           'showUpload' => false
            ]
    ])
    ?>    

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
