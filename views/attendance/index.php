<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Biodata;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attendances';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>

    <?php
    $gridColumns = [
            ['attribute' => 'photo',
            'value' => $searchModel->photo,
            'format' => ['image', ['width' => '100', 'height' => '100']],],
        'name',
        'time',
            ['attribute' => 'status',
            'value' => function ($model) {
                return app\models\Status::findOne($model['status'])->status;
            },],
        'note',
            [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Aksi',
            'class' => 'yii\grid\ActionColumn',
            'template' => '{attend} {official_permission}{personal_permission}{return}{go_home}{sick}{cancel}',
            'buttons' => [
                'attend' => function ($url, $model) {
                    return Html::a('<span></span>Masuk', $url, [
                                'class' => 'btn btn-success btn-xs',
                                'data-toggle' => 'tooltip',
                                'data-method' => 'post',
                                'title' => 'Masuk',
                    ]);
                },
                'official_permission' => function ($url, $model) {
                    return Html::a('<span></span>Izin Dinas', $url, [
                                'class' => 'btn btn-info btn-xs',
                                'data-toggle' => 'tooltip',
                                'data-method' => 'post',
                                'title' => 'Izin Dinas',
                    ]);
                },
                'personal_permission' => function ($url, $model) {
                    return Html::a('<span></span>Izin Pribadi', $url, [
                                'class' => 'btn btn-primary btn-xs',
                                'data-toggle' => 'tooltip',
                                'data-method' => 'post',
                                'title' => 'Izin Pribadi',
                    ]);
                },
                'return' => function ($url, $model) {
                    return Html::a('<span> </span>Kembali', $url, [
                                'class' => 'btn btn-warning btn-xs',
                                'data-toggle' => 'tooltip',
                                'data-method' => 'post',
                                'title' => 'Kembali',
                    ]);
                },
                'go_home' => function ($url, $model) {
                    return Html::a('<span></span>Pulang', $url, [
                                'class' => 'btn btn-warning btn-xs',
                                'data-toggle' => 'tooltip',
                                'data-method' => 'post',
                                'title' => 'Pulang',
                    ]);
                },
                'sick' => function ($url, $model) {
                    return Html::a('<span></span>Sakit', $url, [
                                'class' => 'btn btn-primary btn-xs',
                                'data-toggle' => 'tooltip',
                                'data-method' => 'post',
                                'title' => 'Sakit',
                    ]);
                },
                'cancel' => function ($url, $model) {
                    return Html::a('<span></span>Batal', $url, [
                                'class' => 'btn btn-danger btn-xs',
                                'data-toggle' => 'tooltip',
                                'data-method' => 'post',
                                'title' => 'Batal',
                    ]);
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'attend') {
                    $url = ['oattend', 'id' => $model['user_id']];
                    return $url;
                }
                if ($action === 'official_permission') {
                    $url = ['official', 'id' => $model['user_id']];
                    return $url;
                }
                if ($action === 'cancel') {
                    $url = ['delete', 'id' => $model['id'], 'b' => $model['id']];
                    return $url;
                }
            }
        ],
    ];
    ?>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'hover' => true
    ]);
    ?>
</div>
