<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\City;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\file\FileInput;

$city = ArrayHelper::map(City::find()->select(['id', 'city'])->all(), 'id', 'city');
/* @var $this yii\web\View */
/* @var $model app\models\Proposal */
/* @var $form yii\widgets\ActiveForm */

$script = <<< JS
$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Yakin akan menghapusnya?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Item terhapus!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Batas maksimal");
});
JS;
$this->registerJs($script, 3);
?>

<div class="proposal-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form',]); ?>

    <div class="panel panel-default">

        <div class="panel-heading">Data Institusi / Sekolah / Universitas</div>
        <div class="panel-body">
            <?= $form->field($model, 'institution_name')->textInput(['maxlength' => true]) ?>


            <?= $form->field($model, 'institution_email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'institution_phone')->textInput(['maxlength' => true]) ?>

            <<<<<<< HEAD
            <?=
            $form->field($model, 'city_id')->widget(Select2::classname(), [
                'data' => $city,
                'options' => ['placeholder' => 'Pilih Kabupaten atau Kota'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>

            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Data Magang</div>
        <div class="panel-body">
            <?= $form->field($model, 'start_date')->textInput() ?>

            <?= $form->field($model, 'end_date')->textInput() ?>

            <?=
            $form->field($model, 'letter_file')->widget(FileInput::className(), [
//                'options' => ['accept' => 'document/*'],
                'pluginOptions' => [
//                    'allowedFileExtensions' => ['pdf'],
                    'showUpload' => false
                ]
            ])
            ?>

            <?=
            $form->field($model, 'proposal_file')->widget(FileInput::className(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['pdf'],
                    'showUpload' => false
                ]
            ])
            ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Anggota Magang</div>
        <div class="panel-body">
            <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 20, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.tambah-item', // css class
                'deleteButton' => '.hapus-item', // css class
                'model' => $applicants[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'email',
                ],
            ]);
            ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($applicants as $i => $applicant): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-body">
                            <?php
                            if (!$applicant->isNewRecord) {
                                echo Html::activeHiddenInput($applicant, "[{$i}]id");
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($applicant, "[{$i}]email")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-center down">
                                        <button type="button" class="tambah-item btn btn-success btn-lg"><i class="fa fa-plus"></i></button>
                                        <button type="button" class="hapus-item btn btn-danger btn-lg"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
            <div class="form-group">
                <?= Html::submitButton('<i class="fa fa-check"></i> Simpan', ['class' => $model->isNewRecord ? 'btn btn-success btn-addon' : 'btn btn-primary btn-addon']) ?>
            </div>
        </div>
        =======
        <?=
        $form->field($model, 'referral_letter')->widget(FileInput::className(), [
            'options' => ['accept' => 'proposal/letter/*'],
            'pluginOptions' => [
                'allowedFileExtensions' => ['jpg', 'gif', 'png'],
                'showUpload' => false
            ]
        ])
        ?>    

        <?=
        $form->field($model, 'proposal')->widget(FileInput::className(), [
            'options' => ['accept' => 'proposal/document/*'],
            'pluginOptions' => [
                'allowedFileExtensions' => ['pdf'],
                'showUpload' => false
            ]
        ])
        ?>


        >>>>>>> 5981f0310d01fc5cf3e90644a2ae82098c3f9e34

    </div>

<?php ActiveForm::end(); ?>
</div>
