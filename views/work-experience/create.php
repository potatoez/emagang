<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WorkExperience */

$this->title = 'Tambah Pengalaman Kerja';
$this->params['breadcrumbs'][] = ['label' => 'Pengalaman Kerja', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-experience-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_forms', [
        'models' => $models,
    ]) ?>

</div>
