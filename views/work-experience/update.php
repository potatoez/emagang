<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkExperience */

$this->title = 'Perbarui';
$this->params['breadcrumbs'][] = ['label' => 'Pengalaman Kerja', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Perbarui';
?>
<div class="work-experience-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
