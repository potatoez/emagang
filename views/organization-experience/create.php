<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrganizationExperience */

$this->title = 'Tambah Pengalaman Organisasi';
$this->params['breadcrumbs'][] = ['label' => 'Pengalaman Organisasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-experience-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_forms', [
        'models' => $models,
    ]) ?>

</div>
