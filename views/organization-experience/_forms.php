<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Resep */
/* @var $form yii\widgets\ActiveForm */
// $data = yii\helpers\ArrayHelper::map(app\models\Obat::find()->select(['id_obat', 'nama_obat'] )->all(), 'id_obat', 'nama_obat');

$script = <<< JS
$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Yakin akan menghapusnya?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Item terhapus!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Batas maksimal");
});
JS;
$this->registerJs($script, 3);
?>

<div class="resep-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form',]); ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 20, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.tambah-item', // css class
                'deleteButton' => '.hapus-item', // css class
                'model' => $models[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'organization_name',
                    'position',
                    'year',
                ],
            ]);
            ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($models as $i => $model): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-body">
                            <?php
                            if (!$model->isNewRecord) {
                                echo Html::activeHiddenInput($model, "[{$i}]id");
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($model, "[{$i}]organization_name")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, "[{$i}]position")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-1">
                                    <?= $form->field($model, "[{$i}]year")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-center down">
                                        <button type="button" class="tambah-item btn btn-success btn-lg"><i class="fa fa-plus"></i></button>
                                        <button type="button" class="hapus-item btn btn-danger btn-lg"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
            <div class="form-group">
                <?= Html::submitButton('<i class="fa fa-check"></i> Simpan', ['class' => $model->isNewRecord ? 'btn btn-success btn-addon' : 'btn btn-primary btn-addon']) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
</div>
